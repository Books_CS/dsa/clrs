#include<bits/stdc++.h>
using namespace std;

int main() {

	int n, carry = 0;
	cin >> n;

	int a[n], b[n], c[n+1];

	for(int i = 0;i < n;i++) {
		cin >> a[i];
	}

	for(int i = 0;i < n;i++) {
		cin >> b[i];
	}

	for(int i = 0;i < n+1;i++) {
		c[i] = 0;
	}

	for(int i = n-1;i >= 0;i--) {
		if(a[i] == 0 && b [i] == 0) {
			if(carry == 0) {
				c[i+1] = 0;
			} else {
				c[i+1] = 1;
				carry = 0;
			}
		} else if(a[i] == 0 && b [i] == 1) {
			if(carry == 0) {
				c[i+1] = 1;
			} else {
				c[i+1] = 0;
				carry = 1;
			}
		} else if(a[i] == 1 && b [i] == 0) {
			if(carry == 0) {
				c[i+1] = 1;
			} else {
				c[i+1] = 0;
				carry = 1;
			}
		} else if(a[i] == 1 && b [i] == 1) {
			if(carry == 0) {
				c[i+1] = 0;
				carry = 1;
			} else {
				c[i+1] = 1;
				carry = 1;
			}
		}
	       	//cout << a[i] << b[i] << c[i+1] << endl;
	}
	
	if(carry == 1) {
		c[0] = 1;
	}

	for(int i = 0; i< n+1;i++) {
		cout << c[i];
	}
	cout << endl;

}
