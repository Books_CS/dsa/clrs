#include<bits/stdc++.h>
using namespace std;

int main() {

	int n, key;
	cin >> n >> key;

	int arr[n];

	for(int i = 0;i < n;i++) {
		cin >> arr[i];
	}

	int index = -1;

	for(int i = 0; i< n;i++) {
		if(arr[i] == key) {
			index = i+1;
			break;
		}
	}

	cout << "Key found at : " << index << endl;
	
}
