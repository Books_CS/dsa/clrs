#include <bits/stdc++.h>
using namespace std;

int main() {
	int n,i,j,key;
	cin >> n;

	int arr[n];

	for(i = 0;i < n;i++) {
		cin >> arr[i];
	}

	for(j = 1;j < n;j++) {
		key = arr[j];
		i = j-1;
		while(i > -1 && arr[i] > key) {
			arr[i+1] = arr[i];
			i = i - 1;
		}
		arr[i+1] = key;
	}

	for(int i = 0; i< n-1;i++) {
		cout << arr[i] << " ";
	}
	cout << arr[n-1] << endl;
}
